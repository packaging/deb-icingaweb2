#!/bin/bash

SCRIPT_HOME="$(dirname "$(readlink -f "$0")")"

if ! $SCRIPT_HOME/prepare_test.sh; then
  exit 1
fi

sleep 10

output=`mktemp`

if curl -v http://127.0.0.1/icingaweb2/authentication/login -o "$output"; then
  if grep -q '<div id="login"' "$output"; then
    echo "Login page available"
    exit 0
  else
    echo "Didn't get a logon page from the webserver!"
    echo
    echo "Output of the page is:"
    echo "====================================="
    cat "$output"
    exit 1
  fi
else
  echo "Request for login page failed!"
  echo
  echo "Output of the page is:"
  echo "====================================="
  cat "$output"
  echo "====================================="
  sudo sh -ex <<<'cat /var/log/httpd/*error* /var/log/apache2/*error*'
  exit 1
fi

# vi: ts=2 sw=2 expandtab :
