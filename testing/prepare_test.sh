#!/bin/bash

set -ex

if [ -d archive ]; then
    cd archive
    apt-ftparchive packages . > Packages

    sudo su -c 'echo "deb [trusted=yes] file:$(pwd)/ ./" >>  /etc/apt/sources.list'

    sudo apt-get update -y
fi

sudo apt-get install -y --force-yes icingaweb2 apache2

sudo apache2ctl start || echo "apache2ctl start failed"

